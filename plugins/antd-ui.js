import Vue from 'vue'
import Antd from 'ant-design-vue/lib'
import VeeValidate from 'vee-validate'

Vue.use(Antd)
Vue.use(VeeValidate)
