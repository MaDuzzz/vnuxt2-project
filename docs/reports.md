# Bài tập frontend cơ bản số 1

- *Tác giả: Trần Mạnh Dũng (dungtm3)*

- *Bài tập về Vue2 - framework Nuxt2*

## Câu hỏi chung

### 1. Document Object Model (DOM) là gì?

- DOM là một mô hình được sử dụng trong lập trình web để đại diện cho các phần tử của một trang web và cung cấp phương thức để có thể tương tác với các phần tử một cách thuận tiện và dễ dàng bằng các ngôn ngữ lập trình như JavaScript.
- DOM đại diện cho cấu trúc cây của một trang web, trong đó mỗi phần tử HTML (ví dụ là các thẻ tag: `<div>`, `<p>`, `<input>`,...) được biểu diễn dưới dạng một nút trong cây. Cây này bao gồm một nút gốc, thường là `<html>`, và các nút con tương ứng với các phần tử HTML khác nhau trên trang web. Ví dụ hình dưới đây:
![alt](images/DOM-tree.png)
- Như vậy có thể hiểu trong JavaScript để thao tác được với các thẻ HTML ta phải thông qua đối tượng document (DOM). Với DOM, JavaScript được cung cấp tất cả các sức mạnh cần thiết để tạo ra HTML động, ví dụ như: thêm, sửa, xóa, thay đổi và di chuyển các phần tử trên trang web hay xử lý các sự kiện như khi người dùng click submit-button hay delete-button,...
- REF:
  - <https://viblo.asia/p/nhung-khai-niem-co-ban-ve-dom-DzVkpoDgenW>
  - <https://www.w3schools.com/jsref/dom_obj_document.asp>

### 2. Cách thức Vue có thể manipulate DOM?

#### 2.1 Directives

- Vue cung cấp nhiều directive mạnh mẽ để thao tác với DOM. Ví dụ:
  - `v-bind`: để ràng buộc dữ liệu của một thuộc tính HTML.
  - `v-on`: lắng nghe các sự kiện của DOM.
  - `v-if`, `v-show`, `v-for`: để handle việc hiển thị các phần tử HTML dựa trên điều kiện hay trên tập dữ liệu.

#### 2.2 Lifecycle hooks

- Vue cung cấp các lifecycle hooks như `mounted`, `updated`, `beforeDestroy`,... để có thể thao tác với DOM tại các thời điểm cụ thể trong vòng đời của component.

## Tóm tắt nội dung về các tri thức

### 1. Project Structure của Nuxt2

- Thư mục **assets**: lưu trữ các file hình ảnh, css, font family,...
- Thư mục **components**: Chứa các file .vue là các components - thành phần nhỏ có thể được tái sử dụng nhiều lần trong các pages khác nhau. Ví dụ: Form, Button,...
- Thư mục **pages**: Chứa các views của ứng dụng. Nuxt sẽ đọc tất cả các file .vue trong thư mục này và tự động tạo ra cấu hình router
- Thư mục **plugins**: Chứa các plugins của nuxt hoặc bất kỳ plugin nào cần. Thông thường các plugins được sử dụng để cấu hình thư viện bên thứ ba hoặc thực hiện chức năng theo scope toàn cục, ví dụ: Ant-Design, Vee-Validate,...
- Tệp tin **nuxt.config.js**: Là tệp cấu hình chính của dự án Nuxtjs. Ta có thể khai báo các plugins sẽ được khởi chạy trước khi rendering page, khai báo file global css, cấu hình app title, favicon,...
- Tệp **package.json**: Khai báo các dependencies và devDependencies kèm theo version cụ thể, các câu lệnh build hay start ứng dụng,...

### 2. Routing trong Nuxt2

- Trong nuxt2, hệ thống routing (định tuyến) được quản lý tự động dựa trên cấu trúc thư mục và tệp trong thư mục `pages`. Mỗi tệp hoặc thư mục trong `pages` tương ứng với một route trong ứng dụng.
- Để điều hướng giữa các trang, Nuxt2 cung cấp sẵn component `<nuxt-link>` tương tự `<router-link>` trong Vuejs hay thẻ `<a>` trong HTML thuần.

## Tập lệnh sử dụng

### 1. Chạy code ở môi trường dev

- Pull/Clone code mới nhất trên gitlab repo: `git clone https://gitlab.com/MaDuzzz/vnuxt2-project`
- Cài đặt các package cần thiết (được ghi trong file package.json): `npm i`
- Chạy code ở môi trường dev: `npm run dev`

![alt text](images/npm-run-dev.png)

### 2. Build code

- Chạy câu lệnh `npm run build`

## Link gitlab code base

- Link: <https://gitlab.com/MaDuzzz/vue-nuxt2-course>

- Thông tin phiên bản các gói sử dụng:
  - Nodejs: v16.20.2
  - npm: 8.19.4

![alt](images/project-options.png)
